pkgname=lunaos-gamescope-session-steam
_gitdir=gamescope-session-steam
_commit=151535780654768eff302c9b8d44157273194f75
pkgver=r22.1515357
pkgrel=4
pkgdesc="Steam Big Picture session based on Bazzite gamescope session"
arch=('any')
url="https://github.com/KyleGospo/gamescope-session-steam"
license=('MIT')
groups=("lunaos")
depends=('lunaos-gamescope-session-git' 'yad' 'gamemode' 'lib32-gamemode' 'mangohud' 'lib32-mangohud' 'glfw-x11' 'libxnvctrl' 'jupiter-hw-support' 'steam')
makedepends=('git')
conflicts=('gamescope-session-steam-git' 'gamescope-session-steam-plus-git' 'lunaos-gamescope-session-steam-git')
provides=('lunaos-gamescope-session-steam-git')
replaces=('lunaos-gamescope-session-steam-git')
install=$_gitdir.install
source=("git+https://github.com/KyleGospo/gamescope-session-steam.git#commit=$_commit"
		"0001-lunaos-drop-functions.patch"
	    "0002-lunaos-update-shebang.patch"
	    "0003-lunaos-desktop-return.patch"
	    "0004-lunaos-update-polkit.patch"
	    "0005-lunaos-clean.patch")

sha256sums=('9fc8ead8fe67e2f44884e64c3c9eb708bad57839ffa7e47528b06951d6239c72'
            '0c8f6e7314bfbf63d2b4eeb72011cd60aa9f627be593d1b15d4b3006e8d7f23a'
            '18be9ee1edc864b84d332993e58ffc2e658883439e2b8435bc761f89915227b1'
            '6fec63e580afebcbddff9bcf29a3cbdab5a7403b9da508d86ced947aa054dec9'
            'bf6c97c726504733e15d103b53e0f2cb8b650105b6358ad8b616680bd06dbe40'
            '882a47f6e431e26a4b778293dbfcb424833117fbe9d31ca096fca4d003f29719')

pkgver() {
	cd "$srcdir/${_gitdir}"
	printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  patch -d "$srcdir/${_gitdir}" -p1 -i "${srcdir}"/0001-lunaos-drop-functions.patch
  patch -d "$srcdir/${_gitdir}" -p1 -i "${srcdir}"/0002-lunaos-update-shebang.patch
  patch -d "$srcdir/${_gitdir}" -p1 -i "${srcdir}"/0003-lunaos-desktop-return.patch
  patch -d "$srcdir/${_gitdir}" -p1 -i "${srcdir}"/0004-lunaos-update-polkit.patch
  patch -d "$srcdir/${_gitdir}" -p1 -i "${srcdir}"/0005-lunaos-clean.patch
}

package() {
	cd "$srcdir/${_gitdir}"
	cp -rv ${srcdir}/${_gitdir}/usr ${pkgdir}/usr
	cp -rv ${srcdir}/${_gitdir}/etc ${pkgdir}/etc

    # Avoid file conflict with jupiter-hw-support
    rm ${pkgdir}/usr/bin/jupiter-biosupdate
    rm ${pkgdir}/usr/bin/steamos-polkit-helpers/jupiter-biosupdate
    rm ${pkgdir}/usr/bin/steamos-polkit-helpers/steamos-select-branch
    rm ${pkgdir}/usr/bin/steamos-polkit-helpers/steamos-update

	mkdir -p ${pkgdir}/etc/first-boot/
	install -Dm644 ${srcdir}/${_gitdir}/LICENSE ${pkgdir}/usr/share/licenses/${_gitdir}/LICENSE
}
